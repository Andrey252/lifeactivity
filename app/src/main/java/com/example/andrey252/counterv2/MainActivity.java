// Приложение, демонстрирующее жизненный цикл Активности.
// Рыжкин Андрей 14ИТ18к

package com.example.andrey252.counterv2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    // Тег для ведения логов
    public static final String TAG = "MainActivity";

    // Начальное значение добавленных студентов
    private Integer countOfStudents = 0;

    /**
     * Вызывается при создании Активности
     *
     * @param savedInstanceState восстановление временных данных при изменении Активности.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Введение лога
        Log.d(TAG, "onCreate");
        // Всплывающее уведомление, тосты
        toast("onCreate");
    }

    /**
     * Вызывается, когда Активность стала видимой
     */
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        toast("onStart");
    }

    /**
     * Android вызывает данный обработчик только
     * для Активностей, восстановленных из неактивного состояния
     */
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        toast("onResume");
    }

    /**
     * Вызывается перед выходом из активного состояния
     */
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        toast("onPause");
    }

    /**
     * Вызывается перед выходом из видимого состояния
     */
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
        toast("onStop");
    }

    /**
     * Вызывается перед тем, как Активность снова становится видимой
     */
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
        toast("onRestart");
    }

    /**
     * Вызывается при разрушении активности
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        toast("onDestroy");
    }

    /**
     * Метод сохраняет состояния Активности
     *
     * @param outState временные данные в процессе работы Активности
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("count", countOfStudents);
        Log.d(TAG, "onSaveInstanceState");
        toast("onSaveInstanceState");
    }

    /**
     * Метод восстанавливает состояние приложения после изменении Активности
     *
     * @param savedInstanceState данные для восстановления приложения
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null &&
                savedInstanceState.containsKey("count")) {
            countOfStudents = savedInstanceState.getInt("count");
            outputCountOfStudents();
        }
        Log.d(TAG, "onRestoreInstanceState");
        toast("onRestoreInstanceState");
    }

    /**
     * Метод выводит результат о добавленных студентах, передавая в TextView строку
     */
    public void outputCountOfStudents() {
        TextView counterView = (TextView) findViewById(R.id.txt_counter);
        counterView.setText(String.format(Locale.getDefault(), "%d", countOfStudents));
    }

    /**
     * При вызове метода добавляется студент, счетчик увеличивает значение на 1
     * и сохраняется результат в виде константы <code>countOfStudents</code>
     *
     * @param view вьюшка
     */
    public void onClickButtonAddStudents(View view) {
        countOfStudents++;
        toast("+1");
        outputCountOfStudents();
    }

    /**
     * При вызове метода выводится тост, передавая строку <code>state</code>
     *
     * @param state строка, которая будет использована в тосте.
     */
    public void toast(String state) {
        Toast toast = Toast.makeText(getApplicationContext(),
                state, Toast.LENGTH_SHORT);
        toast.show();
    }
}
